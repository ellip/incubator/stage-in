import os
import sys
from pystac import *
from datetime import datetime, timedelta
from shapely.geometry import Polygon, mapping, shape
from lxml import etree
import gdal
import logging 

gdal.UseExceptions()

logging.basicConfig(stream=sys.stderr, 
                    level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%dT%H:%M:%S')

class UKDMC_II(object):

    def __init__(self, data_dir):
        
        self.data_dir = data_dir
        
        self.bands = [
                        {
                            "name": "nir",
                            "common_name": "nir",
                            "center_wavelength": 0.844, 
                            "eai": 1036,
                            "description": "NIR"
                        },
                        {
                            "name": "red",
                            "common_name": "red",
                            "center_wavelength": 0.668, 
                            "eai": 1561,
                            "description": "Red"
                        },
                        {
                            "name": "green",
                            "common_name": "green",
                            "center_wavelength": 0.559, 
                            "eai": 1811,
                            "description": "Green"
                        }
                    ]
        
        self.common_properties = { 'eo:bands': self.bands, 
                                   'eo:gsd': [22],
                                   'eo:platform': ['uk-dmc'],
                                   'eo:instrument': 'SLIM-6-22'
                                 } 
        
    def get_mtd(self, local_path):

        mtd = None

        for root, dirs, files in os.walk(local_path):
            for file in files:
                if file.endswith('.dim'):
                     mtd = os.path.join(root, file).replace(local_path, '.')

        return mtd
        
    def row_to_item(self, row):

        metadata = self.get_mtd(row['local_path'])
        logging.info(metadata)
        root = etree.parse(os.path.join(row['local_path'], metadata))
        
        proj_meta = root.xpath('/Dimap_Document/Coordinate_Reference_System/Horizontal_CS/HORIZONTAL_CS_CODE', namespaces={'xsi':'noNamespaceSchemaLocation'})[0].text.split(':')[1]
        
        properties={'eo:productType': row['productType'],
                    'eop:wrsLongitudeGrid': row['track']}

        bands = []
        
        tif = root.xpath('/Dimap_Document/Data_Access/Data_File/DATA_FILE_PATH', namespaces={'xsi':'noNamespaceSchemaLocation'})[0].attrib['href']
        
        # stage-in, the row['local_path'] isn't the full path to the .dim or .tif
        ds = gdal.Open(os.path.join(row['local_path'], os.path.dirname(metadata), tif))
        
        geo_transform = ds.GetGeoTransform()
        projection_ref = ds.GetProjectionRef()
        
        hrefs = dict()
        
        for index, band in enumerate(root.xpath('/Dimap_Document/Image_Interpretation/Spectral_Band_Info')):
    
            band_name = band.xpath('BAND_DESCRIPTION', namespaces={'xsi':'noNamespaceSchemaLocation'})[0].text
            gain = float(band.xpath('PHYSICAL_GAIN', namespaces={'xsi':'noNamespaceSchemaLocation'})[0].text)
            offset = float(band.xpath('PHYSICAL_BIAS', namespaces={'xsi':'noNamespaceSchemaLocation'})[0].text)

            for b in self.bands:
        
                if b['name'] == band_name.lower():

                    band = b
            
            output_name = tif.replace('.tif', '_{}.vrt'.format(band_name[0]))
            
            logging.info(os.path.join(row['local_path'], os.path.dirname(metadata), output_name))
            
            vrt_ds = gdal.BuildVRT(os.path.join(row['local_path'], os.path.dirname(metadata), output_name),
                                   os.path.join(row['local_path'], os.path.dirname(metadata), tif),
                                   options=gdal.BuildVRTOptions(bandList=[index + 1]))

            vrt_ds.FlushCache()

            vrt_ds = None

            del(vrt_ds)

            hrefs[band_name.lower()] = os.path.join(os.path.dirname(metadata), output_name)
            
            band['gain'] = gain
            band['offset'] = offset 
    
            bands.append(band)
            
        item = EOItem(id=row['identifier'],
                      geometry=mapping(row['geometry']),
                    bbox=list(row['geometry'].bounds),
                    datetime=datetime.strptime(row['startdate'][0:19], '%Y-%m-%dT%H:%M:%S'),
                    properties=properties, 
                    platform=row['platform'],
                    cloud_cover=float(row['cc']),
                    instrument='SLIM-6-22',
                    bands=bands,
                    gsd=[22], 
                    sun_azimuth=float(root.xpath('/Dimap_Document/Dataset_Sources/Source_Information/Scene_Source/SUN_AZIMUTH', namespaces={'xsi':'noNamespaceSchemaLocation'})[0].text), 
                    sun_elevation=float(root.xpath('/Dimap_Document/Dataset_Sources/Source_Information/Scene_Source/SUN_ELEVATION', namespaces={'xsi':'noNamespaceSchemaLocation'})[0].text),
                     epsg=proj_meta)

        item.add_asset(key='metadata', 
                       asset=Asset(href=metadata, 
                                        media_type='application/xml')) 

        for index, band in enumerate(['nir', 'red', 'green']):

            item.add_asset(key=self.bands[index]['name'], 
                           asset=EOAsset(href=hrefs[band], 
                                         title=self.bands[index]['description'],
                                         media_type=MediaType.GEOTIFF, 
                                         bands=[index]))

        return item

