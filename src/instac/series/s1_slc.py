import os
from pystac import *
from datetime import datetime, timedelta
from shapely.geometry import Polygon, mapping, shape

class S1_SLC(object):

    def __init__(self, data_dir):
        
        self.data_dir = data_dir
        
        self.common_properties = { 'eo:platform': ['sentinel-1a', 'sentinel-1b'],
                                   'eo:instrument': 'sar'
                                 } 
    def get_mtd(self, local_path):

        mtd = None

        for root, dirs, files in os.walk(local_path):
            for file in files:
                if file.endswith('manifest.safe'):
                     mtd = os.path.join(root, file).replace(local_path, '.')

        return mtd
        
    def row_to_item(self, row):

        properties={'eo:productType': row['productType'],
                    'eop:wrsLongitudeGrid': row['track']
                   }

        item = Item(id=row['identifier'],
                    geometry=mapping(row['geometry']),
                    bbox=list(row['geometry'].bounds),
                    datetime=datetime.strptime(row['startdate'][0:19], '%Y-%m-%dT%H:%M:%S'),
                    properties=properties)

        local_path = os.path.join(self.data_dir, row['identifier'])

        item.add_asset(key='metadata', 
                       asset=Asset(href=self.get_mtd(local_path), 
                                        media_type='application/xml')) 

        return item

