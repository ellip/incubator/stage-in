import os
from pystac import *
from datetime import datetime, timedelta
from shapely.geometry import Polygon, mapping, shape

class S3_SLSTR_L1_RBT(object):

    def __init__(self, data_dir):
        
        self.data_dir = data_dir
        
        self.bands = [
                        {
                            "name": "viscal",
                            "description": "VISCAL data obtained from input VISCAL ADF"
                        },
                        {
                            "name": "cartesian_an",
                            "description": "Full resolution cartesian coordinates for the A stripe grid, nadir view"
                        },
                        {
                            "name": "cartesian_ao",
                            "description": "Full resolution cartesian coordinates for the 1km TIR grid, nadir view"
                        },
                        {
                            "name": "cartesian_bn",
                            "description": "16km cartesian coordinates"
                        },
                        {
                            "name": "cartesian_bo",
                            "description": "Global flags for the 1km TIR grid, nadir view"
                        },
                        {
                            "name": "cartesian_fn",
                            "description": "Full resolution geodetic coordinates for the 1km TIR grid, nadir view" 
                        },
                        {
                            "name": "cartesian_fo",
                            "description": "16km geodetic coordinates"
                        },
                        {
                            "name": "cartesian_in",
                            "description": "16km solar and satellite geometry annotations, nadir view"
                        },
                        {
                            "name": "cartesian_io",
                            "description": "Scan, pixel and detector annotations for the 1km TIR grid, nadir view"
                        },
                        {
                            "name": "cartesian_tx",
                            "description": "16km cartesian coordinates"
                        },
                        {
                            "name": "F1_BT_fn",
                            "description": "Gridded pixel brightness temperature for channel F1 (1km F1 grid, nadir view)"
                        },
                        {
                            "name": "F1_BT_fo",
                            "description": "Gridded pixel brightness temperature for channel F1 (1km F1 grid, oblique view)" 
                        },
                        {
                            "name": "F1_quality_fn",
                            "description": "Thermal Infrared quality annotations for channel F1 (1km F1 grid, nadir view)"
                        },
                        {
                            "name": "F1_quality_fo",
                            "description": "Thermal Infrared quality annotations for channel F1 (1km F1 grid, oblique view)"
                        },
                        {
                            "name": "F2_BT_in",
                            "description": "Gridded pixel brightness temperature for channel F2 (1km TIR grid, nadir view)"
                        },
                        {
                            "name": "F2_BT_io",
                            "description": "Gridded pixel brightness temperature for channel F1 (1km TIR grid, oblique view)"
                        },
                        {
                            "name": "F2_quality_in",
                            "description": "Thermal Infrared quality annotations for channel F2 (1km TIR grid, nadir view)"
                        },
                        {
                            "name": "F2_quality_io",
                            "description": "Thermal Infrared quality annotations for channel F2 (1km TIR grid, oblique view)"
                        },
                        {
                            "name": "flags_an",
                            "description": "Global flags for the A stripe grid, nadir view"
                        },
                        {
                            "name": "flags_ao",
                            "description": "Global flags for the A stripe grid, oblique view"
                        },
                        {
                            "name": "flags_an",
                            "description": "Global flags for the B stripe grid, nadir view"
                        },
                        {
                            "name": "flags_ao",
                            "description": "Global flags for the B stripe grid, oblique view"
                        },
                        {
                            "name": "flags_fn",
                            "description": "Global flags for the 1km F1 grid, nadir view"
                        },
                        {
                            "name": "flags_fo",
                            "description": "Global flags for the 1km F1 grid, oblique view"
                        },
                        {
                            "name": "flags_in",
                            "description": "Global flags for the 1km TIR grid, nadir view"
                        },
                        {
                            "name": "flags_io",
                            "description": "Global flags for the 1km TIR grid, oblique view"
                        },
                        {
                            "name": "geodetic_an",
                            "description": "Full resolution geodetic coordinates for the A stripe grid, nadir view"
                        },
                        {
                            "name": "geodetic_ao",
                            "description": "Full resolution geodetic coordinates for the A stripe grid, oblique view"
                        },
                        {
                            "name": "geodetic_bn",
                            "description": "Full resolution geodetic coordinates for the B stripe grid, nadir view"
                        },
                        {
                            "name": "geodetic_bo",
                            "description": "Full resolution geodetic coordinates for the B stripe grid, oblique view"
                        },
                        {
                            "name": "geodetic_fn",
                            "description": "Full resolution geodetic coordinates for the 1km F1 grid, nadir view"
                        },
                        {
                            "name": "geodetic_fo",
                            "description": "Full resolution geodetic coordinates for the 1km F1 grid, oblique view"
                        },
                        {
                            "name": "geodetic_in",
                            "description": "Full resolution geodetic coordinates for the 1km TIR grid, nadir view"
                        },
                        {
                            "name": "geodetic_io",
                            "description": "Full resolution geodetic coordinates for the 1km TIR grid, oblique view"
                        },
                        {
                            "name": "geodetic_tx",
                            "description": "16km geodetic coordinates"
                        },
                        {
                            "name": "geometry_tn",
                            "description": "16km solar and satellite geometry annotations, nadir view"
                        },
                        {
                            "name": "geometry_to",
                            "description": "16km solar and satellite geometry annotations, oblique view"
                        },
                        {
                            "name": "indices_an",
                            "description": "Scan, pixel and detector annotations for the A stripe grid, nadir view"
                        },
                        {
                            "name": "indices_ao",
                            "description": "Scan, pixel and detector annotations for the A stripe grid, oblique view"
                        },
                        {
                            "name": "indices_bn",
                            "description": "Scan, pixel and detector annotations for the B stripe grid, nadir view"
                        },
                        {
                            "name": "indices_bo",
                            "description": "Scan, pixel and detector annotations for the B stripe grid, oblique view"
                        },
                        {
                            "name": "indices_fn",
                            "description": "Scan, pixel and detector annotations for the 1km F1 grid, nadir view"
                        },
                        {
                            "name": "indices_fo",
                            "description": "Scan, pixel and detector annotations for the 1km F1 grid, oblique view"
                        },
                        {
                            "name": "indices_in",
                            "description": "Scan, pixel and detector annotations for the 1km TIR grid, nadir view"
                        },
                        {
                            "name": "indices_io",
                            "description": "Scan, pixel and detector annotations for the 1km TIR grid, oblique view"
                        },
                        {
                            "name": "met_tx",
                            "description": "Meteorological parameters regridded onto the 16km tie points"
                        },
                        {
                            "name": "S1_quality_an",
                            "description": "Visible and Shortwave IR quality annotations for channel S1 (A stripe grid, nadir view)"
                        },
                        {
                            "name": "S1_quality_ao",
                            "description": "Visible and Shortwave IR quality annotations for channel S1 (A stripe grid, oblique view)"
                        },
                        {
                            "name": "S1_radiance_an",
                            "description": "TOA radiance for channel S1 (A stripe grid, nadir view)"
                        },
                        {
                            "name": "S1_radiance_ao",
                            "description": "TOA radiance for channel S1 (A stripe grid, oblique view)"
                        },
                        {
                            "name": "S2_quality_an",
                            "description": "Visible and Shortwave IR quality annotations for channel S2 (A stripe grid, nadir view)"
                        },
                        {
                            "name": "S2_quality_ao",
                            "description": "Visible and Shortwave IR quality annotations for channel S2 (A stripe grid, oblique view)"
                        },
                        {
                            "name": "S2_radiance_an",
                            "description": "TOA radiance for channel S2 (A stripe grid, nadir view)"
                        },
                        {
                            "name": "S2_radiance_ao",
                            "description": "TOA radiance for channel S2 (A stripe grid, oblique view)"
                        },
                        {
                            "name": "S3_quality_an",
                            "description": "Visible and Shortwave IR quality annotations for channel S1 (A stripe grid, nadir view)"
                        },
                        {
                            "name": "S3_quality_ao",
                            "description": "Visible and Shortwave IR quality annotations for channel S1 (A stripe grid, oblique view)"
                        },
                        {
                            "name": "S3_radiance_an",
                            "description": "TOA radiance for channel S1 (A stripe grid, nadir view)"
                        },
                        {
                            "name": "S3_radiance_ao",
                            "description": "TOA radiance for channel S1 (A stripe grid, oblique view)"
                        },
                        {
                            "name": "S4_quality_an",
                            "description": "Visible and Shortwave IR quality annotations for channel S4 (A stripe grid, nadir view)"
                        },
                        {
                            "name": "S4_quality_ao",
                            "description": "Visible and Shortwave IR quality annotations for channel S4 (A stripe grid, oblique view)"
                        },
                        {
                            "name": "S4_quality_bn",
                            "description": "Visible and Shortwave IR quality annotations for channel S4 (B stripe grid, nadir view)"
                        },
                        {
                            "name": "S4_quality_bo",
                            "description": "Visible and Shortwave IR quality annotations for channel S4 (B stripe grid, oblique view)"
                        },
                        {
                            "name": "S4_radiance_an",
                            "description": "TOA radiance for channel S4 (A stripe grid, nadir view)"
                        },
                        {
                            "name": "S4_radiance_ao",
                            "description": "TOA radiance for channel S4 (A stripe grid, oblique view)"
                        },
                        {
                            "name": "S4_radiance_bn",
                            "description": "TOA radiance for channel S4 (B stripe grid, nadir view)"
                        },
                        {
                            "name": "S4_radiance_bo",
                            "description": "TOA radiance for channel S4 (B stripe grid, oblique view)"
                        },
                        {
                            "name": "S5_quality_an",
                            "description": "Visible and Shortwave IR quality annotations for channel S5 (A stripe grid, nadir view)"
                        },
                        {
                            "name": "S5_quality_ao",
                            "description": "Visible and Shortwave IR quality annotations for channel S5 (A stripe grid, oblique view)"
                        },
                        {
                            "name": "S5_quality_bn",
                            "description": "Visible and Shortwave IR quality annotations for channel S5 (B stripe grid, nadir view)"
                        },
                        {
                            "name": "S5_quality_bo",
                            "description": "Visible and Shortwave IR quality annotations for channel S5 (B stripe grid, oblique view)"
                        },
                        {
                            "name": "S5_radiance_an",
                            "description": "TOA radiance for channel S5 (A stripe grid, nadir view)"
                        },
                        {
                            "name": "S5_radiance_ao",
                            "description": "TOA radiance for channel S5 (A stripe grid, oblique view)"
                        },
                        {
                            "name": "S5_radiance_bn",
                            "description": "TOA radiance for channel S5 (B stripe grid, nadir view)"
                        },
                        {
                            "name": "S5_radiance_bo",
                            "description": "TOA radiance for channel S5 (B stripe grid, oblique view)"
                        },
                        {
                            "name": "S6_quality_an",
                            "description": "Visible and Shortwave IR quality annotations for channel S6 (A stripe grid, nadir view)"
                        },
                        {
                            "name": "S6_quality_ao",
                            "description": "Visible and Shortwave IR quality annotations for channel S6 (A stripe grid, oblique view)"
                        },
                        {
                            "name": "S6_quality_bn",
                            "description": "Visible and Shortwave IR quality annotations for channel S6 (B stripe grid, nadir view)"
                        },
                        {
                            "name": "S6_quality_bo",
                            "description": "Visible and Shortwave IR quality annotations for channel S6 (B stripe grid, oblique view)"
                        },
                        {
                            "name": "S6_radiance_an",
                            "description": "TOA radiance for channel S6 (A stripe grid, nadir view)"
                        },
                        {
                            "name": "S6_radiance_ao",
                            "description": "TOA radiance for channel S6 (A stripe grid, oblique view)"
                        },
                        {
                            "name": "S6_radiance_bn",
                            "description": "TOA radiance for channel S6 (B stripe grid, nadir view)"
                        },
                        {
                            "name": "S6_radiance_bo",
                            "description": "TOA radiance for channel S6 (B stripe grid, oblique view)"
                        },
                        {
                            "name": "S7_BT_in",
                            "description": "Gridded pixel brightness temperature for channel S7 (1km TIR grid, nadir view)"
                        },
                        {
                            "name": "S7_BT_io",
                            "description": "Gridded pixel brightness temperature for channel S7 (1km TIR grid, oblique view)"
                        },
                        {
                            "name": "S7_quality_in",
                            "description": "Thermal Infrared quality annotations for channel S7 (1km TIR grid, nadir view)"
                        },
                        {
                            "name": "S7_quality_io",
                            "description": "Thermal Infrared quality annotations for channel S7 (1km TIR grid, oblique view)"
                        },
                        {
                            "name": "S8_BT_in",
                            "description": "Gridded pixel brightness temperature for channel S8 (1km TIR grid, nadir view)"
                        },
                        {
                            "name": "S8_BT_io",
                            "description": "Gridded pixel brightness temperature for channel S8 (1km TIR grid, oblique view)"
                        },
                        {
                            "name": "S8_quality_io",
                            "description": "Thermal Infrared quality annotations for channel S8 (1km TIR grid, oblique view)"
                        },
                        {
                            "name": "S9_BT_in",
                            "description": "Gridded pixel brightness temperature for channel S9 (1km TIR grid, nadir view)"
                        },
                        {
                            "name": "S9_BT_io",
                            "description": "Gridded pixel brightness temperature for channel S9 (1km TIR grid, oblique view)"
                        },
                        {
                            "name": "S9_quality_in",
                            "description": "Thermal Infrared quality annotations for channel S9 (1km TIR grid, nadir view)"
                        },
                        {
                            "name": "S9_quality_io",
                            "description": "Thermal Infrared quality annotations for channel S9 (1km TIR grid, oblique view)"
                        },
                        {
                            "name": "time_an",
                            "description": "Time annotations for the A stripe grid"
                        },
                        {
                            "name": "time_bn",
                            "description": "Time annotations for the B stripe grid"
                        },
                        {
                            "name": "time_in",
                            "description": "Time annotations for the 1 KM grid"
                        }                        
                    ]
        
        self.common_properties = { 'eo:bands': self.bands, 
                                   'eo:gsd': [500, 1000],
                                   'eo:platform': ['sentinel-3a', 'sentinel-3b'],
                                   'eo:instrument': 'SLSTR'
                                 } 
    def get_mtd(self, local_path):

        mtd = None

        for root, dirs, files in os.walk(local_path):
            for file in files:
                if file.endswith('xfdumanifest.xml'):
                     mtd = os.path.join(root, file).replace(local_path, '.')

        return mtd

    def get_local_band_path(self, local_path):
    
        local_bands = dict()

        for root, dirs, files in os.walk(local_path):

            for file in files:
            
                for band in [band['name'] for band in self.bands]:

                    if file == '{}.nc'.format(band):

                        local_bands[band] = os.path.join(root, file).replace(local_path, './')
                        break

        return local_bands
        
    def row_to_item(self, row):

        properties={'eo:productType': row['productType'],
                    'eop:wrsLongitudeGrid': row['track'],
                    'eop:orbitDirection': row['orbitDirection']}

        item = EOItem(id=row['identifier'],
                      geometry=mapping(row['geometry']),
                      bbox=list(row['geometry'].bounds),
                      datetime=datetime.strptime(row['startdate'][0:19], '%Y-%m-%dT%H:%M:%S'),
                      properties=properties, 
                      platform=row['platform'],
                      instrument='SLSTR',
                      bands=self.bands,
                      gsd=[300])

        local_bands = self.get_local_band_path(row['local_path'])

        item.add_asset(key='metadata', 
                       asset=Asset(href=self.get_mtd(row['local_path']), 
                                   media_type='application/xml')) 

        for index, band in enumerate([band['name'] for band in self.bands]):

            item.add_asset(key=band, 
                           asset=EOAsset(href=local_bands[band], 
                                         title=self.bands[index]['description'],
                                         media_type='application/x-netcdf', 
                                         bands=[index]))

        return item

