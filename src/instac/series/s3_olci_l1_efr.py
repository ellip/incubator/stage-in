import os
from pystac import *
from datetime import datetime, timedelta
from shapely.geometry import Polygon, mapping, shape

class S3_OLCI_L1_EFR(object):

    def __init__(self, data_dir):
        
        self.data_dir = data_dir
        
        self.bands = [
                        {
                            "name": "Oa01_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa01"
                        },
                        {
                            "name": "Oa02_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa02"
                        },
                        {
                            "name": "Oa03_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa03"
                        },
                        {
                            "name": "Oa04_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa04"
                        },
                        {
                            "name": "Oa05_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa05"
                        },
                        {
                            "name": "Oa06_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa06"
                        },
                        {
                            "name": "Oa07_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa07"
                        },
                        {
                            "name": "Oa08_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa08"
                        },
                        {
                            "name": "Oa09_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa09"
                        },
                        {
                            "name": "Oa10_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa10"
                        },
                        {
                            "name": "Oa11_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa11"
                        },
                        {
                            "name": "Oa12_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa12"
                        },
                        {
                            "name": "Oa13_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa13"
                        },
                        {
                            "name": "Oa14_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa14"
                        },
                        {
                            "name": "Oa15_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa15"
                        },
                        {
                            "name": "Oa16_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa16"
                        },
                        {
                            "name": "Oa17_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa17"
                        },
                        {
                            "name": "Oa18_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa18"
                        },
                        {
                            "name": "Oa19_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa19"
                        },
                        {
                            "name": "Oa20_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa20"
                        },
                        {
                            "name": "Oa21_radiance",
                            "description": "TOA radiance for OLCI acquisition band Oa21"
                        },
                        {
                            "name": "geo_coordinates",
                            "description": "Geo Coordinates Annotations"
                        },
                        {
                            "name": "instrument_data",
                            "description": "Instrument Annotation"
                        },
                        {
                            "name": "qualityFlags",
                            "description": "Quality flags"
                        },
                        {
                            "name": "removed_pixels",
                            "description": "Removed Pixels information used for SYN L1c reconstruction"
                        },
                        {
                            "name": "tie_geo_coordinates",
                            "description": "Tie-Point Geo Coordinate Annotations"
                        },
                        {
                            "name": "tie_geometries",
                            "description": "Tie-Point Geometries Annotations"
                        },
                        {
                            "name": "tie_meteo",
                            "description": "Tie-Point Meteo Annotations"
                        },
                        {
                            "name": "time_coordinates",
                            "description": "Time Coordinates Annotations"
                        }
                        
                    ]
        
        self.common_properties = { 'eo:bands': self.bands, 
                                   'eo:gsd': [300],
                                   'eo:platform': ['sentinel-3a', 'sentinel-3b'],
                                   'eo:instrument': 'OLCI'
                                 } 
    def get_mtd(self, local_path):

        mtd = None

        for root, dirs, files in os.walk(local_path):
            for file in files:
                if file.endswith('xfdumanifest.xml'):
                     mtd = os.path.join(root, file).replace(local_path, '.')

        return mtd

    def get_local_band_path(self, local_path):
    
        local_bands = dict()

        for root, dirs, files in os.walk(local_path):

            for file in files:
            
                for band in ['Oa01_radiance', 'Oa02_radiance', 'Oa03_radiance', 'Oa04_radiance', 'Oa05_radiance', 'Oa06_radiance', 'Oa07_radiance',
                             'Oa08_radiance', 'Oa09_radiance', 'Oa10_radiance', 'Oa11_radiance', 'Oa12_radiance', 'Oa13_radiance', 'Oa14_radiance',
                             'Oa15_radiance', 'Oa16_radiance', 'Oa17_radiance', 'Oa18_radiance', 'Oa19_radiance', 'Oa20_radiance', 'Oa21_radiance',
                             'geo_coordinates', 'instrument_data', 'qualityFlags', 'removed_pixels', 'tie_geo_coordinates', 'tie_geometries', 'tie_meteo', 'time_coordinates']:

                    if file == '{}.nc'.format(band):

                        local_bands[band] = os.path.join(root, file).replace(local_path, '.')
                        break

        return local_bands
        
    def row_to_item(self, row):

        properties={'eo:productType': row['productType'],
                    'eop:wrsLongitudeGrid': row['track']}

        item = EOItem(id=row['identifier'],
                      geometry=mapping(row['geometry']),
                      bbox=list(row['geometry'].bounds),
                      datetime=datetime.strptime(row['startdate'][0:19], '%Y-%m-%dT%H:%M:%S'),
                      properties=properties, 
                      platform=row['platform'],
                      instrument='OLCI',
                      bands=self.bands,
                      gsd=[300])

        local_bands = self.get_local_band_path(row['local_path'])

        item.add_asset(key='metadata', 
                       asset=Asset(href=self.get_mtd(row['local_path']), 
                                   media_type='application/xml')) 

        for index, band in enumerate(['Oa01_radiance', 'Oa02_radiance', 'Oa03_radiance', 'Oa04_radiance', 'Oa05_radiance', 'Oa06_radiance', 'Oa07_radiance',
                                      'Oa08_radiance', 'Oa09_radiance', 'Oa10_radiance', 'Oa11_radiance', 'Oa12_radiance', 'Oa13_radiance', 'Oa14_radiance',
                                      'Oa15_radiance', 'Oa16_radiance', 'Oa17_radiance', 'Oa18_radiance', 'Oa19_radiance', 'Oa20_radiance', 'Oa21_radiance',
                                      'geo_coordinates', 'instrument_data', 'qualityFlags', 'removed_pixels', 'tie_geo_coordinates', 'tie_geometries', 'tie_meteo', 'time_coordinates']):

            item.add_asset(key=band, 
                           asset=EOAsset(href=local_bands[band], 
                                         title=self.bands[index]['description'],
                                         media_type='application/x-netcdf', 
                                         bands=[index]))

        return item

