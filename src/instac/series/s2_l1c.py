import os
from pystac import *
from datetime import datetime, timedelta
from shapely.geometry import Polygon, mapping, shape

class S2_L1C(object):

    def __init__(self, data_dir):
        
        self.data_dir = data_dir
        
        self.bands = [
                        {
                            "name": "B01",
                            "common_name": "coastal",
                            "center_wavelength": 0.4439,
                            "full_width_half_max": 0.027
                        },
                        {
                            "name": "B02",
                            "common_name": "blue",
                            "center_wavelength": 0.4966,
                            "full_width_half_max": 0.098
                        },
                        {
                            "name": "B03",
                            "common_name": "green",
                            "center_wavelength": 0.56,
                            "full_width_half_max": 0.045
                        },
                        {
                            "name": "B04",
                            "common_name": "red",
                            "center_wavelength": 0.6645,
                            "full_width_half_max": 0.038
                        },
                        {
                            "name": "B05",
                            "center_wavelength": 0.7039,
                            "full_width_half_max": 0.019
                        },
                        {
                            "name": "B06",
                            "center_wavelength": 0.7402,
                            "full_width_half_max": 0.018
                        },
                        {
                            "name": "B07",
                            "center_wavelength": 0.7825,
                            "full_width_half_max": 0.028
                        },
                        {
                            "name": "B08",
                            "common_name": "nir",
                            "center_wavelength": 0.8351,
                            "full_width_half_max": 0.145
                        },
                        {
                            "name": "B8A",
                            "center_wavelength": 0.8648,
                            "full_width_half_max": 0.033
                        },
                        {
                            "name": "B09",
                            "center_wavelength": 0.945,
                            "full_width_half_max": 0.026
                        },
                        {
                            "name": "B10",
                            "center_wavelength": 1.3735,
                            "full_width_half_max": 0.075
                        },
                        {
                            "name": "B11",
                            "common_name": "swir16",
                            "center_wavelength": 1.6137,
                            "full_width_half_max": 0.143
                        },
                        {
                            "name": "B12",
                            "common_name": "swir22",
                            "center_wavelength": 2.22024,
                            "full_width_half_max": 0.242
                        } 
                        
                    ]
        
        self.common_properties = { 'eo:bands': self.bands, 
                                   'eo:gsd': [10, 20, 60],
                                   'eo:platform': ['sentinel-2a', 'sentinel-2b'],
                                   'eo:instrument': 'MSI'
                                 } 
    def get_mtd(self, local_path):

        mtd = None

        for root, dirs, files in os.walk(local_path):
            for file in files:
                if file.endswith('MTD_MSIL1C.xml'):
                     mtd = os.path.join(root, file).replace(local_path, './')

        return mtd

    def get_local_band_path(self, local_path):
    
        local_bands = dict()

        for root, dirs, files in os.walk(local_path):

            for file in files:
            
                for band in [band['name'] for band in self.bands]:

                    if file.endswith('{}.jp2'.format(band)):

                        local_bands[band] = os.path.join(root, file).replace(local_path, './')
                        break

        return local_bands
        
    def row_to_item(self, row):

        utm_zone = row['identifier'].split('_')[5][1:3]
        latitude_band = row['identifier'].split('_')[5][3:4]
        grid_square_x = row['identifier'].split('_')[5][4:5]
        grid_square_y = row['identifier'].split('_')[5][5:6]

        properties={'eo:productType': row['productType'],
                    'eop:wrsLongitudeGrid': row['track'],
                    'proj:epsg': '32{}{}'.format('6' if ((ord(latitude_band.lower()) - 96) > (ord('m') - 96)) else '7',  utm_zone),
                    'eo:cloud_cover': float(row['cc']),
                    's2:tile': row['identifier'].split('_')[5],
                    's2:latitude_band': latitude_band,
                    's2:grid_square_x': grid_square_x,
                    's2:grid_square_y': grid_square_y}

        item = EOItem(id=row['identifier'],
                           geometry=mapping(row['geometry']),
                           bbox=list(row['geometry'].bounds),
                           datetime=datetime.strptime(row['startdate'][0:19], '%Y-%m-%dT%H:%M:%S'),
                           properties=properties, 
                           platform=row['platform'],
                           cloud_cover=float(row['cc']),
                           instrument='S2MSI',
                           bands=self.bands,
                           gsd=[10, 20, 60])

        local_bands = self.get_local_band_path(row['local_path'])

        item.add_asset(key='metadata', 
                       asset=Asset(href=self.get_mtd(row['local_path']), 
                                        media_type='application/xml')) 

        for index, band in enumerate([band['name'] for band in self.bands]):

            item.add_asset(key=band, 
                           asset=EOAsset(href=local_bands[band], 
                                         media_type=MediaType.JPEG2000, 
                                         bands=[index]))

        return item

