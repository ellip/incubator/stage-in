FROM terradue/l1-binder:3.0

MAINTAINER Terradue S.r.l

ENV NB_USER=jovyan \
    NB_UID=1000 \
    NB_GID=100

USER ${NB_USER}

COPY --chown=1000:100 . ${HOME}
#COPY --chown=${NB_USER}:${NB_GID} . ${HOME}

RUN /opt/anaconda/bin/conda env create --file ${HOME}/environment.yml && /opt/anaconda/bin/conda clean -a -y

RUN /opt/anaconda/envs/env_stagein/bin/python -m ipykernel install --name env_stagein

RUN /opt/anaconda/envs/env_stagein/bin/python setup.py install

ENV PREFIX /opt/anaconda/envs/env_stagein

RUN test -f ${HOME}/postBuild && chmod +x ${HOME}/postBuild && ${HOME}/postBuild || exit 0

WORKDIR ${HOME}
