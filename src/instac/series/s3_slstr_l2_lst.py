import os
from pystac import *
from datetime import datetime, timedelta
from shapely.geometry import Polygon, mapping, shape

class S3_SLSTR_L2_LST(object):

    def __init__(self, data_dir):
        
        self.data_dir = data_dir
        
        self.bands = [
                        {
                            "name": "LST_ancillary_ds",
                            "description": "LST ancillary measurement dataset"
                        },
                        {
                            "name": "LST_in",
                            "description": "LST_in Data Set"
                        },
                        {
                            "name": "cartesian_in",
                            "description": "Full resolution cartesian coordinates for the 1km TIR grid, nadir view"
                        },
                        {
                            "name": "cartesian_tx",
                            "description": "16km cartesian coordinates"
                        },
                        {
                            "name": "flags_in",
                            "description": "Global flags for the 1km TIR grid, nadir view"
                        },
                        {
                            "name": "geodetic_in",
                            "description": "Full resolution geodetic coordinates for the 1km TIR grid, nadir view" 
                        },
                        {
                            "name": "geodetic_tx",
                            "description": "16km geodetic coordinates"
                        },
                        {
                            "name": "geometry_tn",
                            "description": "16km solar and satellite geometry annotations, nadir view"
                        },
                        {
                            "name": "indices_in",
                            "description": "Scan, pixel and detector annotations for the 1km TIR grid, nadir view"
                        },
                        {
                            "name": "met_tx",
                            "description": "Meteorological parameters regridded onto the 16km tie points"
                        },
                        {
                            "name": "time_in",
                            "description": "Time annotations for the 1 KM grid"
                        }
                        
                    ]
        
        self.common_properties = { 'eo:bands': self.bands, 
                                   'eo:gsd': [1000],
                                   'eo:platform': ['sentinel-3a', 'sentinel-3b'],
                                   'eo:instrument': 'SLSTR'
                                 } 
    def get_mtd(self, local_path):

        mtd = None

        for root, dirs, files in os.walk(local_path):
            for file in files:
                if file.endswith('xfdumanifest.xml'):
                     mtd = os.path.join(root, file).replace(local_path, '.')

        return mtd

    def get_local_band_path(self, local_path):
    
        local_bands = dict()

        for root, dirs, files in os.walk(local_path):

            for file in files:
            
                for band in [band['name'] for band in self.bands]:

                    if file.endswith('{}.nc'.format(band)):

                        local_bands[band] = os.path.join(root, file).replace(local_path, './')
                        break

        return local_bands
        
    def row_to_item(self, row):

        properties={'eo:productType': row['productType'],
                    'eop:wrsLongitudeGrid': row['track'],
                    'eop:orbitDirection': row['orbitDirection']}

        item = EOItem(id=row['identifier'],
                      geometry=mapping(row['geometry']),
                      bbox=list(row['geometry'].bounds),
                      datetime=datetime.strptime(row['startdate'][0:19], '%Y-%m-%dT%H:%M:%S'),
                      properties=properties, 
                      platform=row['platform'],
                      instrument='SLSTR',
                      bands=self.bands,
                      gsd=[300])

        local_bands = self.get_local_band_path(row['local_path'])

        item.add_asset(key='metadata', 
                       asset=Asset(href=self.get_mtd(row['local_path']), 
                                   media_type='application/xml')) 

        for index, band in enumerate([band['name'] for band in self.bands]):

            item.add_asset(key=band, 
                           asset=EOAsset(href=local_bands[band], 
                                         title=self.bands[index]['description'],
                                         media_type='application/x-netcdf', 
                                         bands=[index]))

        return item

