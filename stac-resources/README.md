## Resources found in the Internet for STAC

### Sentinel-1

[Sentinel-1 GRD ARD collection](sentinel-1/COPERNICUS_S1_GRD.json) source  https://earthengine-stac.storage.googleapis.com/catalog/COPERNICUS_S1_GRD.json see https://developers.google.com/earth-engine/sentinel1 for info

### Sentinel-2

[L1C item](sentinel-2/S2B_9VXK_20171013_0.json) source https://sentinel-stac.s3.amazonaws.com/sentinel-2-l1c/9/V/XK/2017-10-13/S2B_9VXK_20171013_0.json
[L2A_item](sentinel-2/sentinel2-sample.json) source https://github.com/radiantearth/stac-spec/blob/master/item-spec/examples/sentinel2-sample.json
[L1C collection](sentinel-2/s2l1c-collection.json) source https://sentinel-stac.s3.amazonaws.com/sentinel-2-l1c/catalog.json
[L1C collection](sentinel-2/sentinel2l1c.json) source https://github.com/radiantearth/stac-spec/blob/master/collection-spec/examples/sentinel2.json
[L1C collection](sentinel-2/COPERNICUS_S2.json) source https://earthengine-stac.storage.googleapis.com/catalog/COPERNICUS_S2.json
[L2A collection](sentinel-2/COPERNICUS_S2_SR.json)source https://earthengine-stac.storage.googleapis.com/catalog/COPERNICUS_S2_SR.json
[L2A collection](sentinel-2/sentinel-s2-l2a.json) source https://github.com/sat-utils/sat-stac-sentinel/blob/master/stac_sentinel/sentinel-s2-l2a.json
[L1C collection](sentinel-2/sentinel-s2-l1c.json) source https://github.com/sat-utils/sat-stac-sentinel/blob/master/stac_sentinel/sentinel-s2-l1c.json

### Sentinel-3

[OLCI collection](sentinel-3/COPERNICUS_S3_OLCI.json) source https://earthengine-stac.storage.googleapis.com/catalog/COPERNICUS_S3_OLCI.json


### Landsat-8

[item](landsat-8/landsat-item.json) source  https://github.com/radiantearth/stac-spec/blob/master/collection-spec/examples/landsat-item.json
[item](landsat-8/landsat8-sample.json) source https://github.com/radiantearth/stac-spec/blob/master/item-spec/examples/landsat8-sample.json
[collection](landsat-8/collection) source https://github.com/radiantearth/stac-spec/blob/master/collection-spec/examples/landsat-collection.json
[collection](landsat-8/LANDSAT_LC08_C01_T1_TOA.json) source https://earthengine-stac.storage.googleapis.com/catalog/LANDSAT_LC08_C01_T1_TOA.json

### ASTER 

[collection L1T](aster/ASTER_AST_L1T_003.json) source https://earthengine-stac.storage.googleapis.com/catalog/ASTER_AST_L1T_003.json

### ENVISAT ASAR

[ASAR GM](envisat.json) source https://github.com/radiantearth/stac-spec/blob/master/extensions/sar/examples/envisat.json

### Other resources

Sentinel-1 GRD ARD https://developers.google.com/earth-engine/sentinel1
