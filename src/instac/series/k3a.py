import os
from pystac import *
from datetime import datetime, timedelta
from shapely.geometry import Polygon, mapping, shape
from lxml import etree

class KOMPSAT_3A(object):

    def __init__(self, data_dir):
        
        self.data_dir = data_dir
        
        self.bands = [
                        {
                            "name": "MS1",
                            "common_name": "blue",
                            "center_wavelength": 0.49, 
                            "gain": 0.0362,
                            "offset": -22.1416,
                            "eai": 2001,
                            "description": "MS1 (Blue)"
                        },
                        {
                            "name": "MS2",
                            "common_name": "green",
                            "center_wavelength": 0.559, 
                            "gain": 0.0249, 
                            "offset": -14.0210,
                            "eai": 1875,
                            "description": "MS2 (Green)"
                        },
                        {
                            "name": "MS3",
                            "common_name": "red",
                            "center_wavelength": 0.668, 
                            "gain": 0.0229, 
                            "offset": -13.0457,
                            "eai": 1525,
                            "description": "MS3 (Red)"
                        },
                        {
                            "name": "MS4",
                            "common_name": "nir",
                            "center_wavelength": 0.844, 
                            "gain": 0.013, 
                            "offset": -10.2834,
                            "eai": 1027,
                            "description": "MS4 (NIR)"
                        },
                        {
                            "name": "PAN",
                            "common_name": "pan",
                            "center_wavelength": 0.675,
                            "full_width_half_max": 0.225,
                            "description": "PAN"
                        }
                    ]
        
        self.common_properties = { 'eo:bands': self.bands, 
                                   'eo:gsd': [0.54, 2.16],
                                   'eo:platform': ['kompsat-3a'],
                                   'eo:instrument': 'AEISS-A'
                                 } 
        
    def get_mtd(self, local_path):

        mtd = None

        for root, dirs, files in os.walk(local_path):
            for file in files:
                if file.endswith('_Aux.xml'):
                     mtd = os.path.join(root, file).replace(local_path, '.')

        return mtd

    def get_local_band_path(self, local_path):
    
        local_bands = dict()

        for root, dirs, files in os.walk(local_path):

            for file in files:
            
                for band in ['B', 'G', 'R', 'N', 'P']:
                    
                    if file.endswith('_L1G_{}.tif'.format(band)):
                        
                        local_bands[band] = os.path.join(root, file).replace(local_path, '.')
                        break

        return local_bands
        
    def row_to_item(self, row):

        metadata = self.get_mtd(row['local_path'])
        
        root = etree.parse(os.path.join(row['local_path'], metadata))
        
        proj_meta = root.xpath('/Auxiliary/General/Projection/Parameter', namespaces={'xsi':'noNamespaceSchemaLocation'})[0].text
        
        properties={'eo:productType': row['productType'],
                    'eop:wrsLongitudeGrid': row['track']}

        bands = []
        for band in self.bands:
            
            if band['name'] == 'PAN':
                
                bands.append(band)
                continue
                
            band['offset'] = float(root.xpath('/Auxiliary/Image/{}/RadianceConversion/Offset'.format(band['name']))[0].text)
            
            # DN to radiance uses L = DN * gain + offset instead of L = DN / gain + offset
            band['gain'] = 1 / float(root.xpath('/Auxiliary/Image/{}/RadianceConversion/Gain'.format(band['name']))[0].text)
        
            bands.append(band)
            
        item = EOItem(id=row['identifier'],
                      geometry=mapping(row['geometry']),
                    bbox=list(row['geometry'].bounds),
                    datetime=datetime.strptime(row['startdate'][0:19], '%Y-%m-%dT%H:%M:%S'),
                    properties=properties, 
                    platform=row['platform'],
                    cloud_cover=float(row['cc']),
                    instrument='AEISS-A',
                    bands=bands,
                    gsd=[0.54, 2.16], 
                    sun_azimuth=float(root.xpath('/Auxiliary/Metadata/MetadataBlock/SunAngle/Azimuth', namespaces={'xsi':'noNamespaceSchemaLocation'})[0].text), 
                    sun_elevation=float(root.xpath('/Auxiliary/Metadata/MetadataBlock/SunAngle/Elevation', namespaces={'xsi':'noNamespaceSchemaLocation'})[0].text),
                     epsg='326{}'.format(proj_meta[1:3]) if proj_meta[0] == 'N' else '327{}'.format(proj_meta[1:3]))

        local_bands = self.get_local_band_path(row['local_path'])

        item.add_asset(key='metadata', 
                       asset=Asset(href=metadata, 
                                        media_type='application/xml')) 

        for index, band in enumerate(['B', 'G', 'R', 'N', 'P']):

            item.add_asset(key=bands[index]['name'], 
                           asset=EOAsset(href=local_bands[band], 
                                         title=self.bands[index]['description'],
                                         media_type=MediaType.GEOTIFF, 
                                         bands=[index]))

        return item

