import os
import sys
import logging 
import pandas as pd
from .series import S1_SLC, S1_GRD, S2_L1C, S2_L2A, S3_OLCI_L1_EFR, S3_OLCI_L2_LFR, S3_SLSTR_L1_RBT, S3_SLSTR_L2_LST, KOMPSAT_3A, UKDMC_II
from pystac import Catalog, CatalogType, SpatialExtent, TemporalExtent, Collection
from datetime import datetime, timedelta
from shapely.geometry import shape
from shapely.wkt import loads
import geopandas as gpd
import cioppy
import shutil 

logging.basicConfig(stream=sys.stderr, 
                    level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%dT%H:%M:%S')


@pd.api.extensions.register_dataframe_accessor('to_stac_multicollection')
class StacWritterM:
    def __init__(self, pandas_obj):
        self._validate(pandas_obj)
        self._obj = pandas_obj
           
    @staticmethod
    def _validate(obj):
 
        missing_fields = []
        
        for field in ['startdate', 'identifier', 'geometry', 'collection']:
            
            if field not in obj.columns:
            
                missing_fields.append(field)    
        
        if len(missing_fields) != 0: 
            
            raise AttributeError('The dataframe must have at least the columns {}.'.format(missing_fields))
            
    def to_stac_multicollection(self, catalog_id, catalog_description, root_href, data_dir):
        
        self._validate(self._obj)
        
        catalog = Catalog(id=catalog_id, 
                          description=catalog_description)
        
        if self._obj.collection.unique()[0] is not None:
        
            for collection in self._obj.collection.unique():

                items = []

                bboxes = []


                for index, row in self._obj[self._obj.collection == collection].iterrows():

                    if row.productType == 'S2MSI1C':

                        to_stac = S2_L1C(data_dir)

                    elif row.productType == 'S2MSI2A':

                        to_stac = S2_L2A(data_dir)

                    elif row.productType == 'OL_1_EFR___':

                        to_stac = S3_OLCI_L1_EFR(data_dir)

                    elif row.productType == 'OL_2_LFR___':

                        to_stac = S3_OLCI_L2_LFR(data_dir)

                    elif row.productType == 'SL_1_RBT___':

                        to_stac = S3_SLSTR_L1_RBT(data_dir)

                    elif row.productType == 'SL_2_LST___':

                        to_stac = S3_SLSTR_L2_LST(data_dir)

                    elif row.productType == 'SLC' and row.platform in ['S1A', 'S1B']:

                        to_stac = S1_SLC(data_dir)

                    elif row.productType == 'GRD' and row.platform in ['S1A', 'S1B']:

                        to_stac = S1_GRD(data_dir)

                    elif row.platform in ['KOMPSAT3A', 'KOMPSAT-3A']:

                        to_stac = KOMPSAT_3A(data_dir)

                    elif row.platform in ['UK-DMC-2']:

                        to_stac = UKDMC_II(data_dir)

                    else: 
                        raise ValueError

                    item = to_stac.row_to_item(row)   

                    items.append(item)

                    bboxes.append(shape(item.geometry).bounds)



                spatial_extent = SpatialExtent(bboxes=bboxes)


                min_date = datetime.strptime(min(self._obj[self._obj.collection == collection]['startdate'])[0:19], '%Y-%m-%dT%H:%M:%S')
                max_date = datetime.strptime(max(self._obj[self._obj.collection == collection]['enddate'])[0:19], '%Y-%m-%dT%H:%M:%S')

                if min_date == max_date:

                    collection_interval = [min_date, 
                                           max_date + timedelta(0,1)]
                else:

                    collection_interval = [min_date, 
                                           max_date]


                temporal_extent = TemporalExtent(intervals=[list(dict.fromkeys(collection_interval))])


                collection_extent = Extent(spatial=spatial_extent, 
                                                temporal=temporal_extent)

                collection = Collection(id=row.collection,
                                        title=row.collection_title,
                                        description=row.collection_description,
                                        extent=collection_extent,
                                        properties=to_stac.common_properties,
                                        license='CC-BY-SA-4.0',
                                        stac_extensions=['eo'])

                collection.add_items(items)

                catalog.add_child(collection)

                to_stac = None

            

        else:
            
            items = []
            
            for index, row in self._obj.iterrows():

                if row.productType == 'S2MSI1C':

                    to_stac = S2_L1C(data_dir)

                elif row.productType == 'S2MSI2A':

                    to_stac = S2_L2A(data_dir)

                elif row.productType == 'OL_1_EFR___':

                    to_stac = S3_OLCI_L1_EFR(data_dir)

                elif row.productType == 'OL_2_LFR___':

                    to_stac = S3_OLCI_L2_LFR(data_dir)

                elif row.productType == 'SL_1_RBT___':

                    to_stac = S3_SLSTR_L1_RBT(data_dir)

                elif row.productType == 'SL_2_LST___':

                    to_stac = S3_SLSTR_L2_LST(data_dir)

                elif row.productType == 'SLC' and row.platform in ['S1A', 'S1B']:

                    to_stac = S1_SLC(data_dir)

                elif row.productType == 'GRD' and row.platform in ['S1A', 'S1B']:

                    to_stac = S1_GRD(data_dir)

                elif row.platform in ['KOMPSAT3A', 'KOMPSAT-3A']:

                    to_stac = KOMPSAT_3A(data_dir)

                elif row.platform in ['UK-DMC-2']:

                    to_stac = UKDMC_II(data_dir)

                else: 
                    raise ValueError

                item = to_stac.row_to_item(row)   

                items.append(item)
                
                to_stac = None

            catalog.add_items(items)
    
    
        catalog.describe()

        catalog.normalize_and_save(root_href=root_href,
                                      catalog_type=CatalogType.SELF_CONTAINED)
            
        return os.path.join(root_href, 'catalog.json')   
    
    __call__ = to_stac_multicollection


class VirtualCatalog(object):
    
    def __init__(self, catalog_definition):
        
        self.catalog_definition = catalog_definition
    
    def stage(self, data_dir):
    
        searches = []
    
        if 'collections' in self.catalog_definition['catalog'].keys():
    
            for collection in self.catalog_definition['catalog']['collections']: 

                target_data_dir = os.path.join(data_dir, list(collection.keys())[0])

                logging.info(target_data_dir)

                for key, value in collection.items():

                    logging.info('Stage-in collection {}'.format(key))

                    search = self.get_references(value['entries'],
                                                 target_data_dir,
                                                 True)

                    search['collection'] = key
                    search['collection_title'] = value['title']
                    search['collection_description'] = value['description']
                    searches.append(search)
        
        else:
            
            target_data_dir = data_dir
            
            logging.info(target_data_dir)
            
  
            logging.info('Stage-in')

            search = self.get_references(self.catalog_definition['catalog']['entries'],
                                         target_data_dir,
                                         True)

            search['collection'] = None
            search['collection_title'] = None
            search['collection_description'] = None
            searches.append(search)
            

        search = pd.concat(searches, ignore_index=True)
        
        catalog = search.to_stac_multicollection(catalog_id=self.catalog_definition['catalog']['id'],
                                                 catalog_description=self.catalog_definition['catalog']['description'],
                                                 root_href=data_dir,
                                                 data_dir=target_data_dir)

        return Catalog.from_file(catalog)
    
    @staticmethod
    def check_folder(folder):
    
        if os.path.exists(folder) and os.path.isdir(folder):

            shutil.rmtree(folder)
            
        if not os.path.exists(folder):
            
            os.makedirs(folder)

    def get_references(self, references, data_path, stage_in=True):

    
        temp_searches = []

        ciop = cioppy.Cioppy()

        if len(references) == 1 and stage_in:

            logging.info('Stage-in product to {}'.format(data_path))

        if len(references) > 1 and stage_in:

            logging.info('Stage-in {} products to {}'.format(len(references), 
                                                                 data_path))

        for index, reference in enumerate(references):

            logging.info('The input reference (#{} of {}) is: {}'.format(index+1,
                                                                         len(references),
                                                                         reference))

            search_params = dict()
            search_params['do'] = 'terradue'

            search_temp = ciop.search(end_point=reference,
                                      params=search_params,
                                      output_fields='self,productType,track,enclosure,identifier,wkt,startdate,enddate,cc,platform,orbitDirection',
                                      model='EOP',
                                      timeout=50000)

            if stage_in:

                target_path = os.path.join(data_path, search_temp[0]['identifier'])
                
                self.check_folder(target_path)
     
                logging.info('Retrieve {} from {}'.format(search_temp[0]['identifier'], 
                                                          search_temp[0]['enclosure']))

                logging.info('Target local folder: {}'.format(target_path))
        
                local_path = ciop.copy(search_temp[0]['enclosure'], 
                                       target_path)

                assert(local_path)
                
                logging.info('Staged {}'.format(local_path))
                
                search_temp = gpd.GeoDataFrame(search_temp)

                search_temp['local_path'] = target_path
                
            else:
                search_temp[0]['local_path'] = '{}/{}'.format(target_path, search_temp[0]['identifier'])
                search_temp = gpd.GeoDataFrame(search_temp)


            temp_searches.append(search_temp)

        search = gpd.GeoDataFrame(pd.concat(temp_searches, ignore_index=True)) 

        search['geometry'] = search['wkt'].apply(loads)
        search['cc'] = pd.to_numeric(search['cc'])
        #search['startdate'] = pd.to_datetime(search['startdate'])
        #search['enddate'] = pd.to_datetime(search['enddate'])

        return search
