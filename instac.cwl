$graph:
- baseCommand: stage-in
  class: CommandLineTool
  hints:
    DockerRequirement:
      dockerPull: eoepca-stage-in:0.3
  id: stagein
  inputs:
    inp1:
      inputBinding:
        position: 1
        prefix: -t
      type: string
    inp2:
      inputBinding:
        position: 2
        prefix: -u
      type: string
    inp3:
      inputBinding:
        position: 3
        prefix: -p
      type: string
    inp4:
      inputBinding:
        position: 4
      type: string[]
  outputs:
    results:
      outputBinding:
        glob: .
      type: Any
  requirements:
    EnvVarRequirement:
      envDef:
        PATH: /opt/anaconda/envs/env_stagein/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    ResourceRequirement: {}
  stderr: std.err
  stdout: std.out
- class: Workflow
  label: Instac stage-in
  doc: Stage-in using Instac
  id: main
  inputs:
    input_reference:
      doc: A reference to an opensearch catalog
      label: A reference to an opensearch catalog
      type: string[]
    target_folder:
      label: Folder to stage-in 
      doc: Folder to stage-in 
      type: string
    store_username:
      type: string
    store_apikey:
      type: string
  outputs:
  - id: wf_outputs
    outputSource:
    - node_1/results
    type:
      items: Directory
      type: array
  steps:
    node_1:
      in:
        inp1: target_folder
        inp2: store_username
        inp3: store_apikey
        inp4: input_reference
      out:
      - results
      run: '#stagein'
cwlVersion: v1.0

