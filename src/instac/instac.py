import os
import sys
import click
import logging
import yaml
import requests
from pystac import Catalog, STAC_IO, CatalogType
from json import JSONDecodeError
from .vcatalog import VirtualCatalog
from .stac import my_read_method

logging.basicConfig(stream=sys.stderr, 
                    level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%dT%H:%M:%S')


    
@click.command()
@click.option('--target', '-t', 'data_dir', help='target directory')
@click.option('--catalog', '-c', 'catalog_definition', default=None, help='catalog definition')
@click.option('--store_username', '-u', 'store_username', default=None, help='store username')
@click.option('--store_apikey', '-p', 'store_apikey', default=None, help='store username')
@click.argument('input_references', nargs=-1, required=False)
def main(data_dir, catalog_definition, input_references, store_username, store_apikey):

    if store_username is not None:
        
        print(store_username)
        os.environ['STAGEIN_USERNAME'] = store_username
        os.environ['STAGEIN_PASSWORD'] = store_apikey
        print(os.environ['STAGEIN_USERNAME'])
        
    STAC_IO.read_text_method = my_read_method
    
    print(input_references[0])

    try: 
        
        cat = Catalog.from_file(input_references[0])
        cat.describe()
        
        items = []
        
        for item in cat.get_items():
    
            items.append(item)
        
        catalog = Catalog(id='catalog',
                  description='staged STAC catalog')

        catalog.add_items(items)

        catalog.normalize_and_save(root_href=data_dir,
                                   catalog_type=CatalogType.RELATIVE_PUBLISHED)
        
        catalog.describe()
        print(catalog)
        
        sys.exit(0)
        
    except (JSONDecodeError, KeyError) as e:

        print(e)

        pass
        
    
    if not os.path.exists(data_dir):
    
        os.mkdir(data_dir)

    if catalog_definition is not None:
        
        with open(catalog_definition) as file:

            cat_definition = yaml.load(file)
            
    else:
        
        cat_definition = {'catalog': {'id': 'catid',
                          'title': 'cat title',
                          'description': 'this is the cat description',
                          'entries': list(input_references)}}
    
    stac_in = VirtualCatalog(cat_definition)
    
    catalog = stac_in.stage(data_dir)
    
    print(catalog)
    
if __name__ == "__main__":
    main()
