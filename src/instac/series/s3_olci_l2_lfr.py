import os
from pystac import *
from datetime import datetime, timedelta
from shapely.geometry import Polygon, mapping, shape

class S3_OLCI_L2_LFR(object):

    def __init__(self, data_dir):
        
        self.data_dir = data_dir
        
        self.bands = [
                        {
                            "name": "geo_coordinates",
                            "description": "Geo Coordinates Annotations"
                        },
                        {
                            "name": "instrument_data",
                            "description": "Instrument Annotation"
                        },
                        {
                            "name": "iwv",
                            "description": "Integrated water vapour column"
                        },
                        {
                            "name": "lqsf",
                            "description": "Land Quality and Science Flags"
                        },
                        {
                            "name": "ogvi",
                            "description": "OLCI global Vegetal Index"
                        },
                        {
                            "name": "otci",
                            "description": "OLCI Terrestrial Chlorophyll Index" 
                        },
                        {
                            "name": "rc_ogvi",
                            "description": "Rectified Reflectance"
                        },
                        {
                            "name": "tie_geo_coordinates",
                            "description": "Tie-Point Geo Coordinate Annotations"
                        },
                        {
                            "name": "tie_geometries",
                            "description": "Tie-Point Geometries Annotations"
                        }
                        
                    ]
        
        self.common_properties = { 'eo:bands': self.bands, 
                                   'eo:gsd': [300],
                                   'eo:platform': ['sentinel-3a', 'sentinel-3b'],
                                   'eo:instrument': 'OLCI'
                                 } 
    def get_mtd(self, local_path):

        mtd = None

        for root, dirs, files in os.walk(local_path):
            for file in files:
                if file.endswith('xfdumanifest.xml'):
                     mtd = os.path.join(root, file).replace(local_path, '.')

        return mtd

    def get_local_band_path(self, local_path):
    
        local_bands = dict()

        for root, dirs, files in os.walk(local_path):

            for file in files:
            
                for band in ['geo_coordinates', 'instrument_data', 'iwv', 'lqsf', 'ogvi', 'otci', 'rc_ogvi', 'tie_geo_coordinates', 'tie_geometries']:

                    if file == '{}.nc'.format(band):
                        
                        local_bands[band] = os.path.join(root, file).replace(local_path, '.')
                        break

        return local_bands
        
    def row_to_item(self, row):

        properties={'eo:productType': row['productType'],
                    'eop:wrsLongitudeGrid': row['track']}

        item = EOItem(id=row['identifier'],
                           geometry=mapping(row['geometry']),
                           bbox=list(row['geometry'].bounds),
                           datetime=datetime.strptime(row['startdate'][0:19], '%Y-%m-%dT%H:%M:%S'),
                           properties=properties, 
                           platform=row['platform'],
                           instrument='OLCI',
                           bands=self.bands,
                           gsd=[300])

        local_bands = self.get_local_band_path(row['local_path'])

        item.add_asset(key='metadata', 
                       asset=Asset(href=self.get_mtd(row['local_path']), 
                                   media_type='application/xml')) 

        for index, band in enumerate(['geo_coordinates', 'instrument_data', 'iwv', 'lqsf', 'ogvi', 'otci', 'rc_ogvi', 'tie_geo_coordinates', 'tie_geometries']):
 
            item.add_asset(key=band, 
                           asset=EOAsset(href=local_bands[band], 
                                         title=self.bands[index]['description'],
                                         media_type='application/x-netcdf', 
                                         bands=[index]))

        return item

